{-# LANGUAGE EmptyDataDecls    #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving      #-}

module Handler.DicoDB where

import Data.Text
import Database.Persist.Sqlite
import Prelude
import Control.Monad.IO.Class  (liftIO)
import Database.Persist
import Database.Persist.TH

share [mkPersist sqlSettings, mkMigrate "migrateDico"] [persistLowerCase|
DicoDB sql=dico
    fr Text sql=fr
    vo Text sql=vo
    deriving Show
|]

pravasConf = SqliteConf { sqlDatabase = "./dico.db"
                        , sqlPoolSize = 2
}

runDicoDB f = runSqlite (sqlDatabase pravasConf) $ do
  runMigration migrateDico
  f

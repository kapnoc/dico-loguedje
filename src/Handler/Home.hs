{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Home where

import Import
import Yesod.Form.Bootstrap3 (BootstrapFormLayout (..), renderBootstrap3)
import Text.Julius (RawJS (..))

import qualified Data.Text as T

import Handler.DicoDB
import Control.Monad.IO.Class  (liftIO)

import Debug.Trace

-- Define our data that will be used for creating the form.
data DicoForm = DicoForm
    { query :: Text
    }

data DicoResult = DicoResult Text Text


-- This is a handler function for the GET request method on the HomeR
-- resource pattern. All of your resource patterns are defined in
-- config/routes
--
-- The majority of the code you will write in Yesod lives in these handler
-- functions. You can spread them across multiple files if you are so
-- inclined, or create a single monolithic file.
getHomeR :: Handler Html
getHomeR = do
    (formWidget, formEnctype) <- generateFormPost sampleForm
    let submission = Nothing :: Maybe DicoForm
        handlerName = "getHomeR" :: Text
        results = [] :: [Entity DicoDB]

    defaultLayout $ do
        aDomId <- newIdent
        setTitle "Dico Loguèdje"
        $(widgetFile "homepage")

postHomeR :: Handler Html
postHomeR = do
    ((formResult, formWidget), formEnctype) <- runFormPost sampleForm
    let handlerName = "postHomeR" :: Text
        submission = case formResult of
            FormSuccess res -> Just res
            _ -> Nothing
    -- let results = [] :: [DicoResult]
    let t = case submission of
          Just (DicoForm t) -> t
          _ -> ""
    results <- runDicoDB $ selectList [Filter DicoDBFr (Left $ T.concat ["%", t, "%"]) (BackendSpecificFilter "LIKE")] [Asc DicoDBId]

    defaultLayout $ do
        aDomId <- newIdent
        setTitle "Dico Loguèdje"
        $(widgetFile "homepage")

sampleForm :: Form DicoForm
sampleForm = renderBootstrap3 BootstrapBasicForm $ DicoForm
    <$> areq textField textSettings Nothing
    -- Add attributes like the placeholder and CSS classes.
    where textSettings = FieldSettings
            { fsLabel = "Françâs -> Patois"
            , fsTooltip = Nothing
            , fsId = Nothing
            , fsName = Nothing
            , fsAttrs =
                [ ("class", "form-control")
                , ("placeholder", "Françâs")
                ]
            }

{-# LANGUAGE PackageImports #-}
import "dico-loguedje" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
